import React, {Component} from 'react';
class ErrorBoundary extends Component {
    state ={
        hasError: false,
        errorMessage: ''
    }

    componentDiCatch = (error, info) => {
        this.setState({hasError: true, errorMessage: error});
    }
    render() {
        if(this.state.hasError){
        return <h1>Sometimg went wrong</h1>;
        }
        else{
            return this.props.children;
        }
    }
}

export default ErrorBoundary;