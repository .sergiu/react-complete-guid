import React, { Component } from 'react';
import classess from './App.css';
import Cockpit from '../components/Cockpit/Cockpit';
import Persons from '../components/Persons/Persons';

class App extends Component  {
  constructor(props)
  {
    super(props);
    console.log('[App.js] constructor');
  }

  state = {
    persons: [
      { id: 1, name: 'Max', age:28 },
      { id: 2, name: 'Manu', age:29 },
      { id: 3, name: 'Sergiu', age:33 }
    ],
      username: 'write text', 
      showPersons: false
  }

  static getDerivedStateFromProps(props, state)
  {
    console.log('[App.js] getDerivedStateFromProps', props);
    return state;
  }

  // componentWillMount() {
  //   console.log('[App.js] componentWillMount');
  // }

  componentDidMount() {
    console.log('[App.js] componentDidMount');
  }

  shouldComponentUpdate(nextProps, nextState)
  {
    console.log('[App.js] shouldComponentUpdate');
    return true;
  }

  nameChangedHandler =(event, id) =>{
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = { 
      ...this.state.persons[personIndex]
    };

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({ persons: persons  });
    }

    deletePersonHandler = (personIndex) => {
      //const persons = this.state.persons.slice();
      const persons = [...this.state.persons];
      persons.splice(personIndex, 1);
      this.setState({persons: persons});
    };

    togglePersonHandler = () => {
      const doesShow = this.state.showPersons;
      this.setState({ showPersons: !doesShow });
    }

    userNameChangeHandler = (event) =>{
      this.setState({
        username: event.target.value
      });
    }

    render(){
      console.log('[App.js] render');

      let persons = null;

      if ( this.state.showPersons ){
        persons = <Persons 
            persons={this.state.persons}
            clicked={this.deletePersonHandler}
            changed = {this.nameChangedHandler} />
      }

      return (
          <div className={classess.App}>
           <Cockpit
              title = {this.props.appTitle} 
              showPersons={this.state.showPersons}
              persons ={this.state.persons}
              clicked = {this.togglePersonHandler} />
          {persons}
          </div>
      );
  };
  //return React.createElement('div', null, React.createElement('h1', {className: 'App'}, 'I\'m a new react app!!!!'));
  }
export default App;
